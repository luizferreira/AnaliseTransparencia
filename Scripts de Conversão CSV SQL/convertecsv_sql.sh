while getopts f:t:s: flag
do 
    case $flag in
        f) csv=$OPTARG;;
        t) table=$OPTARG;;
	s) separador=$OPTARG;;
    esac
done

colunas=$(head --lines=1 $csv | sed "s/$separador/,/g")

sql="$(echo $csv | cut -d "." -f 1).sql"

rm $sql

tail --lines=+2 $csv | while read l 
do
    valores="'$(echo $l | sed "s/$separador/','/g"| sed 's/"//g' | tr -d "\n\r")'"
    echo "INSERT INTO $table VALUES ($valores, uuid());"
done > $sql  
