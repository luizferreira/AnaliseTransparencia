import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscal.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

for i in range(len(headers)):
	print(headers[i], i)
	
fileEvento = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalEvento.csv', encoding='ISO-8859-1')
arquivoEvento = csv.reader(fileEvento, delimiter = ';')
headers = []
headers = next(arquivoEvento)

for i in range(len(headers)):
	print(headers[i], i)

colunasDeInteresse = [0, 2, 3, 5, 7, 17, 20, 18, 100, 4, 6, 8, 13, 19]
numeros = [2, 3, 20]
booleans = [17, 18]
datas = [5, 7]

format_data = "%d/%m/%Y %H:%M:%S"

rows = []
for row in arquivoCSV:
	
	string = "INSERT INTO Nota_Fiscal VALUES ("
	for coluna in colunasDeInteresse:
	
		if(coluna == 100):
			string += "\"" + str(uuid.uuid1()) + "\", "
			
		elif(coluna == 6):
			if row[coluna] == "Autorização de Uso":
				string += "NULL, "
			
			else:
				for registro in arquivoEvento:
					if(str(row[0]) == str(registro[0])):
						#print("Chaves Iguais! {0}".format(row[0]))
						string += "\"" + str(registro[8]) + "\", "
						fileEvento = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalEvento.csv', encoding='ISO-8859-1')
						arquivoEvento = csv.reader(fileEvento, delimiter = ';')
						headers = next(arquivoEvento)
						break
		else:	
			valorString = str(row[coluna])
			
			if(coluna in numeros):
				string += valorString.replace(",", ".")
				
			elif(coluna in datas):
				date = datetime.strptime(valorString, format_data)
				string += "\"" + str(date) + "\""
				
			elif(coluna in booleans):
				if(coluna == 17):
					if (valorString == "1 - OPERAÇÃO INTERNA"):
						string += "1"
						
					else:
						string += "0"
				
				if(coluna == 18):
					if (valorString == "1 - CONSUMIDOR FINAL"):
						string += "1"
						
					else:
						string += "0"				
			
			else:
				string += "\"" + valorString + "\""
			
			if(coluna != colunasDeInteresse[-1]):
				string += ","
			
	string += ");\n"
	
	rows.append(string)

arquivoInserts = open("./Insert_Nota_Fiscal.sql", "w")

for linha in rows:
	arquivoInserts.write(linha)

arquivoInserts.close()
file.close()
