import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalItem.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

for i in range(len(headers)):
	print(headers[i], i)

campos_desejados = [23, 24, 19, 20, 22, 25, -1]
numeros = [23, 20, 25]

produtos = []

arquivoInserts = open("./Insert_Produto.sql", "w")

for row in arquivoCSV:
	descricao = row[19]
	if descricao in produtos:
		continue
	
	string = "INSERT INTO Produto VALUES ("
	for element in campos_desejados:
		if element in numeros:
			string += str(row[element].replace(",", ".")) + ""
		elif element == -1:
			string += "\"" + str(uuid.uuid1()) + "\""
		else:
			string += "\"" + str(row[element] + "\"")
			
		if element != campos_desejados[-1]:
			string += ", "
			
	string += ");\n"
	arquivoInserts.write(string)
	produtos.append(descricao)
	
arquivoInserts.close()
