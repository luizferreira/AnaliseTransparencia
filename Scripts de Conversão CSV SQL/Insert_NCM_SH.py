import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalItem.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

#for i in range(len(headers)):
#	print(headers[i], i)

campos_desejados = [20, 21]
cadastros = []

arquivoInserts = open("./Insert_NCM_SH_Tipo.sql", "w")

for row in arquivoCSV:
    cadastro = row[21]
    if cadastro not in cadastros:
        string = "Insert INTO NCM_SH_Tipo VALUES (" + str(row[20] + ", \"" + str(row[21]) + "\");\n")
        arquivoInserts.write(string)
        cadastros.append(cadastro)

arquivoInserts.close()


