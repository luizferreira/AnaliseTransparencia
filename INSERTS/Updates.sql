UPDATE Nota_Fiscal JOIN Natureza_Operacao ON Nota_Fiscal.fk_natureza_operacao_id = Natureza_Operacao.descricao SET Nota_Fiscal.fk_natureza_operacao_id = Natureza_Operacao.id;
UPDATE Nota_Fiscal JOIN Emitente ON Nota_Fiscal.fk_emitente_id = Emitente.CPF_CNPJ SET Nota_Fiscal.fk_emitente_id = Emitente.id;
UPDATE Nota_Fiscal JOIN Destinatario ON Nota_Fiscal.fk_destinatario_id = Destinatario.cnpj SET Nota_Fiscal.fk_destinatario_id = Destinatario.id;
UPDATE Nota_Fiscal JOIN Presenca_Comprador ON Nota_Fiscal.fk_presenca_comprador_id = Presenca_Comprador.tipo_Presenca SET Nota_Fiscal.fk_presenca_comprador_id = Presenca_Comprador.id;

UPDATE ProdutoNotaFiscal JOIN Nota_Fiscal ON ProdutoNotaFiscal.fk_Nota_Fiscal_ID = Nota_Fiscal.chave_de_Acesso SET ProdutoNotaFiscal.fk_Nota_Fiscal_ID = Nota_Fiscal.id;
UPDATE ProdutoNotaFiscal JOIN Produto ON ProdutoNotaFiscal.fk_Produto_ID = Produto.descricao SET ProdutoNotaFiscal.fk_Produto_ID = Produto.id;

UPDATE Evento_NotaFiscal JOIN Evento ON Evento_NotaFiscal.evento_id = Evento.protocolo SET Evento_NotaFiscal.evento_id = Evento.id
UPDATE Evento_NotaFiscal JOIN Nota_Fiscal ON Evento_NotaFiscal.notaFiscal_id = Nota_Fiscal.chave_de_Acesso SET Evento_NotaFiscal.notaFiscal_id = Nota_Fiscal.id
