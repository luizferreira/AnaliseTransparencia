-- 1 com select

-- OK Seleciona notas fiscais por periodo emissao
SELECT nf.chave_de_Acesso AS chave, nf.serie AS serie, nf.numero AS numero, nf.data_Emissao AS dataEmissao, nf.valor AS valor
from Nota_Fiscal nf 
where nf.data_Emissao > <data_inicio>
and nf.data_Emissao < <data_fim>

-- 1 com subselect

-- OK Seleciona Notas fiscais e o valor gasto com todos os Produtos que tenham a descricao informada

SELECT nf.chave_de_Acesso AS chave, p.descricao AS produto, nf_prod.valor_total AS valorTotal
FROM Nota_Fiscal nf
JOIN Produto_NotaFiscal nf_prod ON nf.id = nf_prod.fk_Nota_Fiscal_id
JOIN Produto p ON nf_prod.fk_Produto_id = p.id
WHERE p.id IN (
	SELECT p.id
	FROM Produto p
	WHERE p.descricao LIKE concat('%', '<busca_produto>', '%')
);

-- 2 com junção de apenas 2 tabelas

-- OK Quantidade de Notas Fiscais emitidas por cada Natureza de Operação
SELECT nop.descricao AS natureza, count(nf.id) AS quantidade
FROM Natureza_Operacao nop
JOIN Nota_Fiscal nf ON nop.id = nf.fk_natureza_operacao_id
WHERE nop.descricao = <natureza_selecionada> 
GROUP BY nop.descricao
ORDER BY nop.descricao;

-- OK Valor total gasto em cada Produto (Com junção externa)
SELECT p.descricao AS produto, sum(nf_prod.valor_total) AS valorTotal, count(nf_prod.fk_Produto_id) AS quantidade
FROM Produto p
LEFT JOIN Produto_NotaFiscal nf_prod ON p.id = nf_prod.fk_Produto_id
WHERE p.descricao LIKE CONCAT('%', <produto_desejado>, '%')
GROUP BY p.descricao
ORDER BY p.descricao;

-- 2 com junção de 3+ tabelas 

-- OK Dez Uniões Federativas com maiores movimentações em Notas Fiscais
SELECT sigla, sum(valorTotal) AS valorTotal FROM 

	(SELECT UF.sigla AS sigla, sum(nf_prod.valor_total) AS valorTotal
	FROM Produto_NotaFiscal nf_prod
	JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id
	JOIN Emitente e ON nf.fk_Emitente_id = e.id
	JOIN Municipio m ON e.municipio_id = m.id
	JOIN UF ON m.uf_id = UF.id
	WHERE UF.sigla = <sigla>
	GROUP BY UF.id

	UNION

	SELECT UF. sigla AS sigla, sum(nf_prod.valor_total) as valorTotal
	FROM Produto_NotaFiscal nf_prod
	JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id
	JOIN Destinatario d ON nf.fk_destinatario_id = d.id
	JOIN UF ON d.uf_id = UF.id
	WHERE UF.sigla = <sigla>
	GROUP BY UF.id) 
	
AS uniao
GROUP BY sigla
LIMIT 10;

-- OK Valor total em Notas Fiscais que cada Destinatario pagou

SELECT d.nome AS destinatario, sum(nf_prod.valor_total) AS valorPago
FROM Destinatario d
JOIN Nota_Fiscal nf ON d.id = nf.fk_destinatario_id
JOIN Produto_NotaFiscal nf_prod ON nf.id = nf_prod.fk_Nota_Fiscal_id
WHERE d.nome = <nome_informado>
GROUP BY d.nome
ORDER BY d.nome;

-- 1 com operação sobre conjunto

-- OK Total em Notas Fiscais emitidas e recebidas por uma UF
SELECT sum(nf_prod.valor_total) AS valorTotal
FROM Produto_NotaFiscal nf_prod
JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id
JOIN Emitente e ON nf.fk_Emitente_id = e.id
JOIN Municipio m ON e.municipio_id = m.id
JOIN UF ON m.uf_id = UF.id
WHERE UF.sigla = <sigla>
GROUP BY UF.id

UNION

SELECT sum(nf_prod.valor_total)
FROM Produto_NotaFiscal nf_prod
JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id
JOIN Destinatario d ON nf.fk_destinatario_id = d.id
JOIN UF ON d.uf_id = UF.id
WHERE UF.sigla = <sigla>
GROUP BY UF.id

--3 com agregação

-- OK Media gasta com Produtos de um determinado tipo
SELECT tipo.tipo_de_produto AS tipoProduto, avg(nf_prod.valor_total) AS media
FROM NCM_SH_Tipo tipo
JOIN Produto p ON tipo.id = p.fk_codigo_NCM_SH
JOIN Produto_NotaFiscal nf_prod ON p.id = nf_prod.fk_Produto_id
WHERE tipo.tipo_de_produto = <tipoProduto_escolhido>
GROUP BY tipo.tipo_de_produto;

-- OK Valor total gasto com um determinado emitente

SELECT e.razao_Social AS emitente, sum(nf_prod.valor_total) AS valorGasto
FROM Emitente e
JOIN Nota_Fiscal nf ON e.id = nf.fk_Emitente_id
JOIN Produto_NotaFiscal nf_prod ON nf.id = nf_prod.fk_Nota_Fiscal_id
WHERE e.razao_Social = <razao_social_informada>
GROUP BY e.CPF_CNPJ, e.razao_Social;

-- OK Quantas Notas Fiscais um Emitente fez para um Destinatário

SELECT e.razao_Social AS emitente, d.nome AS destinatario, count(1) AS quantidade
FROM Emitente e
JOIN Nota_Fiscal nf ON e.id = nf.fk_Emitente_id
JOIN Destinatario d ON nf.fk_destinatario_id = d.id
WHERE e.razao_Social LIKE CONCAT('%', <razao_social_informada>, '%') 
AND d.nome LIKE CONCAT('%', <nome_informado>, '%')
GROUP BY e.razao_Social, d.nome
ORDER BY quantidade DESC;
