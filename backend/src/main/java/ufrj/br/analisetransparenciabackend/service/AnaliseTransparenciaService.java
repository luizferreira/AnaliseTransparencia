package ufrj.br.analisetransparenciabackend.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ufrj.br.analisetransparenciabackend.model.dto.*;
import ufrj.br.analisetransparenciabackend.repository.AnaliseTransparenciaRepository;

@Service
public class AnaliseTransparenciaService {

    @Autowired
    private AnaliseTransparenciaRepository analiseTransparenciaRepository;

    public List<ProdutoDTO> recuperarProdutoPorValorMaior(Float valor) 
    {
        return analiseTransparenciaRepository.recuperarProdutosComValorMaior(valor);
    }
    
    public List<NotaFiscalDTO> recuperarNotasFiscaisPorDescricaoProduto(String descricao) 
    {
    	return analiseTransparenciaRepository.recuperarNotasFiscaisPorDescricaoProduto(descricao);
    }
    
    public List<QuantidadeNotasNaturezaoperacaoDTO> recuperarQuantidadeNotasPorNaturezaOperacao()
    {
    	return analiseTransparenciaRepository.recuperarQuantidadeNotasPorNaturezaOperacao();
    }
    
    public List<TotalGastoPorUFDTO> recuperarDezUFsQueMaisGastaram()
    {
    	return analiseTransparenciaRepository.recuperarDezUFsQueMaisGastaram();
    }
    
    public List<QuantidadeNotaFiscalEmitenteDestinarioDTO> recuperarQuantasNotasFiscaisEmitenteFezParaDestinatario(String emitente, String destinatario)
    {
    	return analiseTransparenciaRepository.recuperarQuantasNotasFiscaisEmitenteFezParaDestinatario(emitente,  destinatario);
    }
    
    public List<String> recuperarEmitentes(String emitente)
    {
    	return analiseTransparenciaRepository.recuperarEmitentes(emitente);
    }
    
    public List<String> recuperarDestinatario(String destinatario)
    {
    	return analiseTransparenciaRepository.recuperarDestinatarios(destinatario);
    }
    
    public List<EmitenteDTO> recuperarValorGastoPorEmitente(String emitente)
    {
    	return analiseTransparenciaRepository.recuperarValorGastoPorEmitente(emitente);
    }

    public List<String> recuperarNomesNaturezaOperacaoAutocomplete(String busca){
        return analiseTransparenciaRepository.recuperarNaturezaOperacao(busca);

    }

    public List<NaturezaOperacaoNfDTO> recuperarQuantidadeNotasFiscaisPorNaturezaOpercao(String naturezaOp)
    {
        return analiseTransparenciaRepository.recuperarQuantidadeNotasFiscaisPorNaturezaOpercao(naturezaOp);

    }
    
    public List<TotalPagoPorDestinatarioDTO> receuperarTotalPagoPorDestinatario(String destinatario)
    {
    	return analiseTransparenciaRepository.receuperarTotalPagoPorDestinatario(destinatario);
    }
    
    public List<String> recuperarTipoProduto(String busca){
        return analiseTransparenciaRepository.recuperarTipoProduto(busca);

    }
    
    public List<MediaGastaPorTipoProdutoDTO> recuperarMediaGastaPorTipoProduto(String tipoProduto)
    {
    	return analiseTransparenciaRepository.recuperarMediaGastaPorTipoProduto(tipoProduto);
    }
    
    public List<EstadoDTO> recuperarEstados()
    {
    	return analiseTransparenciaRepository.recuperarEstados();
    }
    
    public List<ProdutoDTO> recuperarProdutosComValorTotalGasto(String produto)
    {
    	return analiseTransparenciaRepository.recuperarProdutosComValorTotalGasto(produto);
    }
    
    public List<NotaFiscalDTO> recuperarNotasFiscaisPorPeriodo(Date dataInicio, Date dataFim)
    {
    	return analiseTransparenciaRepository.recuperarNotasFiscaisPorPeriodo(dataInicio, dataFim);
    }

    public ValoresEmitidosERecebidosDTO recuperarValoresEmitidosERecebidosPorUF(String sigla){
        List<RetornoFloatDTO> resultados = analiseTransparenciaRepository.recuperarValoresEmitidosERecebidosPorUF(sigla);
        ValoresEmitidosERecebidosDTO valores = new ValoresEmitidosERecebidosDTO();

        valores.setValorEmitido(resultados.get(0).getValorTotal());
        valores.setValorRecebido(resultados.get(1).getValorTotal());

        return valores;

    }

    public NotaFiscalProdutoDTO recuperarNotaFiscalCompleta(String chave) {

        NotaFiscalProdutoDTO notaFiscalProdutoDTO = new NotaFiscalProdutoDTO();

        notaFiscalProdutoDTO.setNotaFiscal(analiseTransparenciaRepository.recuperarNotaFiscalCompleta(chave).get(0));
        notaFiscalProdutoDTO.setProdutos(analiseTransparenciaRepository.recuperarProdutosDeNotaFiscal(chave));

        return notaFiscalProdutoDTO;

    }
}
