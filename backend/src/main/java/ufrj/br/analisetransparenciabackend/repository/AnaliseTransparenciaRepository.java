package ufrj.br.analisetransparenciabackend.repository;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ufrj.br.analisetransparenciabackend.model.dto.*;

@Repository
public class AnaliseTransparenciaRepository {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public List<ProdutoDTO> recuperarProdutosComValorMaior(Float valor) 
	{
		String sql = " SELECT p.descricao AS produto, p.unidade AS unidade, p.valor_unitario AS valorUnitario " +
					 " FROM Produto p " +
					 " WHERE p.valor_unitario >= :valor";
		
		return jdbcTemplate.queryForStream(sql, Map.of("valor", valor), BeanPropertyRowMapper.newInstance(ProdutoDTO.class)).toList();		
		
	}
	
	public List<NotaFiscalDTO> recuperarNotasFiscaisPorDescricaoProduto(String descricao) 
	{
		String sql = " SELECT nf.chave_de_Acesso AS chave, p.descricao AS produto, nf_prod.valor_total AS valorTotal " +
				     " FROM Nota_Fiscal nf " +
				     " JOIN Produto_NotaFiscal nf_prod " +
				     " ON nf.id = nf_prod.fk_Nota_Fiscal_id " +
				     " JOIN Produto p " +
				     " ON nf_prod.fk_Produto_id = p.id " +
				     " WHERE p.id IN ( SELECT p.id " +
					 "                 FROM Produto p " +
					 "                 WHERE p.descricao LIKE CONCAT('%', :descricao, '%') )";
		
		return jdbcTemplate.queryForStream(sql, Map.of("descricao", descricao), BeanPropertyRowMapper.newInstance(NotaFiscalDTO.class)).toList();
	}
	
	public List<QuantidadeNotasNaturezaoperacaoDTO> recuperarQuantidadeNotasPorNaturezaOperacao() 
	{
		String sql = "SELECT nop.descricao AS naturezaOperacao, count(nf.id) AS quantidade "
				+ "FROM Natureza_Operacao nop "
				+ "JOIN Nota_Fiscal nf "
				+ "ON nop.id = nf.fk_natureza_operacao_id "
				+ "GROUP BY nop.id "
				+ "ORDER BY nop.descricao";
		
		return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(QuantidadeNotasNaturezaoperacaoDTO.class));
	}
	
	public List<TotalGastoPorUFDTO> recuperarDezUFsQueMaisGastaram()
	{
		String sql =
			"SELECT sigla, sum(valorTotal) AS valorTotal FROM " +
				"(SELECT UF.sigla AS sigla, sum(nf_prod.valor_total) AS valorTotal " +
				"FROM Produto_NotaFiscal nf_prod " +
				"JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id " +
				"JOIN Emitente e ON nf.fk_Emitente_id = e.id " +
				"JOIN Municipio m ON e.municipio_id = m.id " +
				"JOIN UF ON m.uf_id = UF.id " +
				"GROUP BY UF.id " +
				"UNION " +
				"SELECT UF.sigla AS sigla, sum(nf_prod.valor_total) AS valorTotal " +
				"FROM Produto_NotaFiscal nf_prod " +
				"JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id " +
				"JOIN Destinatario d ON nf.fk_destinatario_id = d.id " +
				"JOIN UF ON d.uf_id = UF.id " +
				"GROUP BY UF.id) " +
			"AS tabelaUniao " +
			"GROUP BY sigla " +
			"ORDER BY valorTotal DESC " +
			"LIMIT 10";
		
		return jdbcTemplate.query(sql,  BeanPropertyRowMapper.newInstance(TotalGastoPorUFDTO.class));
	}
	
	public List<QuantidadeNotaFiscalEmitenteDestinarioDTO> recuperarQuantasNotasFiscaisEmitenteFezParaDestinatario(String emitente, String destinatario)
	{
		emitente = emitente == null ? "" : emitente;
		destinatario = destinatario == null ? "" : destinatario;
		
		String sql = "SELECT e.razao_Social AS emitente, d.nome AS destinatario, count(1) AS quantidade "
				+ "FROM Emitente e "
				+ "JOIN Nota_Fiscal nf ON e.id = nf.fk_Emitente_id "
				+ "JOIN Destinatario d ON nf.fk_destinatario_id = d.id "
				+ "WHERE e.razao_Social LIKE CONCAT('%', :emitente, '%') "
				+ "AND d.nome LIKE CONCAT('%', :destinatario, '%') "
				+ "GROUP BY e.razao_Social, d.nome "
				+ "ORDER BY quantidade DESC";
		
		return jdbcTemplate.queryForStream(sql, Map.of("emitente", emitente, "destinatario", destinatario), 
				BeanPropertyRowMapper.newInstance(QuantidadeNotaFiscalEmitenteDestinarioDTO.class)).toList();
	}
	
	public List<String> recuperarEmitentes(String emitente)
	{
		String sql = "SELECT e.razao_Social AS razao FROM Emitente e WHERE e.razao_Social LIKE CONCAT('%', :emitente, '%')";
		
		return jdbcTemplate.queryForList(sql, Map.of("emitente", emitente), String.class);
	}
	
	public List<String> recuperarDestinatarios(String destinatario)
	{
		String sql = "SELECT d.nome AS nome FROM Destinatario d WHERE d.nome LIKE CONCAT('%', :destinatario, '%')";
		
		return jdbcTemplate.queryForList(sql, Map.of("destinatario", destinatario), String.class);
	}
	
	public List<EmitenteDTO> recuperarValorGastoPorEmitente(String emitente) 
	{
		String sql = "SELECT e.razao_Social AS emitente, sum(nf_prod.valor_total) AS valorGasto "
				+ "FROM Emitente e "
				+ "JOIN Nota_Fiscal nf ON e.id = nf.fk_Emitente_id "
				+ "JOIN Produto_NotaFiscal nf_prod ON nf.id = nf_prod.fk_Nota_Fiscal_id "
				+ "WHERE e.razao_Social = :emitente "
				+ "GROUP BY e.CPF_CNPJ, e.razao_Social ";
		
		return jdbcTemplate.queryForStream(sql, Map.of("emitente", emitente), BeanPropertyRowMapper.newInstance(EmitenteDTO.class)).toList();
	}

	public List<String> recuperarNaturezaOperacao(String busca) 
	{
		String sql = "SELECT nop.descricao AS descricao " +
		"FROM Natureza_Operacao nop " +
		"WHERE nop.descricao LIKE CONCAT('%', :busca, '%'); ";

		return jdbcTemplate.queryForList(sql, Map.of("busca", busca), String.class);
	}

	public List<NaturezaOperacaoNfDTO> recuperarQuantidadeNotasFiscaisPorNaturezaOpercao(String naturezaOp)
	{

		String sql = "SELECT nop.descricao AS natureza, count(nf.id) AS quantidade " +
		"FROM Natureza_Operacao nop " +
		"JOIN Nota_Fiscal nf " +
		"ON nop.id = nf.fk_natureza_operacao_id " +
		"WHERE nop.descricao = :naturezaOp " +
		"GROUP BY nop.descricao " +
		"ORDER BY nop.descricao";

		return  jdbcTemplate.queryForStream(sql, Map.of("naturezaOp", naturezaOp), BeanPropertyRowMapper.newInstance(NaturezaOperacaoNfDTO.class)).toList();
	}
	
	public List<TotalPagoPorDestinatarioDTO> receuperarTotalPagoPorDestinatario(String destinatario)
	{
		String sql = "SELECT d.nome AS destinatario, sum(nf_prod.valor_total) AS valorPago "
				+ "FROM Destinatario d "
				+ "JOIN Nota_Fiscal nf ON d.id = nf.fk_destinatario_id "
				+ "JOIN Produto_NotaFiscal nf_prod ON nf.id = nf_prod.fk_Nota_Fiscal_id "
				+ "WHERE d.nome = :destinatario "
				+ "GROUP BY d.nome "
				+ "ORDER BY d.nome ";
				
		return jdbcTemplate.queryForStream(sql, Map.of("destinatario", destinatario), BeanPropertyRowMapper.newInstance(TotalPagoPorDestinatarioDTO.class)).toList();
	}
	
	public List<String> recuperarTipoProduto(String tipoProduto)
	{
		String sql = "SELECT tipo.tipo_de_produto "
				+ "FROM NCM_SH_Tipo tipo "
				+ "WHERE tipo.tipo_de_produto LIKE CONCAT('%', :tipoProduto, '%')";
		
		return jdbcTemplate.queryForList(sql, Map.of("tipoProduto", tipoProduto), String.class);
	}
	
	public List<MediaGastaPorTipoProdutoDTO> recuperarMediaGastaPorTipoProduto(String tipoProduto)
	{
		String sql = "SELECT tipo.tipo_de_produto AS tipoProduto, avg(nf_prod.valor_total) AS media "
				+ "FROM NCM_SH_Tipo tipo "
				+ "JOIN Produto p ON tipo.id = p.fk_codigo_NCM_SH "
				+ "JOIN Produto_NotaFiscal nf_prod ON p.id = nf_prod.fk_Produto_id "
				+ "WHERE tipo.tipo_de_produto = :tipoProduto "
				+ "GROUP BY tipo.tipo_de_produto ";
		
		return jdbcTemplate.queryForStream(sql, Map.of("tipoProduto", tipoProduto), BeanPropertyRowMapper.newInstance(MediaGastaPorTipoProdutoDTO.class)).toList();
	}
	
	public List<EstadoDTO> recuperarEstados()
	{
		String sql = "SELECT u.sigla as sigla, u.id as id, u.bandeira as bandeira, u.nome as nome "
				+ "FROM UF u ORDER BY u.sigla";
		
		return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(EstadoDTO.class));
	}
	
	public List<ProdutoDTO> recuperarProdutosComValorTotalGasto(String produto)
	{
		String sql = "SELECT p.descricao AS produto, sum(nf_prod.valor_total) AS valorTotal, count(nf_prod.fk_Produto_id) AS quantidade "
				+ "FROM Produto p "
				+ "LEFT JOIN Produto_NotaFiscal nf_prod ON p.id = nf_prod.fk_Produto_id "
				+ "WHERE p.descricao LIKE CONCAT('%', :produto, '%') "
				+ "GROUP BY p.descricao "
				+ "ORDER BY p.descricao ";
		
		return jdbcTemplate.queryForStream(sql, Map.of("produto", produto), BeanPropertyRowMapper.newInstance(ProdutoDTO.class)).toList();
	}
	
	public List<NotaFiscalDTO> recuperarNotasFiscaisPorPeriodo(Date dataInicio, Date dataFim)
	{
		String sql = "SELECT nf.chave_de_Acesso AS chave, nf.serie AS serie, nf.numero AS numero, nf.data_Emissao AS dataEmissao, nf.valor AS valor "
				+ "from Nota_Fiscal nf "
				+ "where nf.data_Emissao > :dataInicio "
				+ "and nf.data_Emissao < :dataFim ";
		
		return jdbcTemplate.queryForStream(sql, Map.of("dataInicio", dataInicio, "dataFim", dataFim), BeanPropertyRowMapper.newInstance(NotaFiscalDTO.class)).toList();
	}

	public List<RetornoFloatDTO> recuperarValoresEmitidosERecebidosPorUF(String sigla){
		String sql =
		"SELECT sum(nf_prod.valor_total) AS valorTotal " +
		"FROM Produto_NotaFiscal nf_prod " +
		"JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id " +
		"JOIN Emitente e ON nf.fk_Emitente_id = e.id " +
		"JOIN Municipio m ON e.municipio_id = m.id " +
		"JOIN UF ON m.uf_id = UF.id " +
		"WHERE UF.sigla = :sigla " +
		"GROUP BY UF.id " +
		"UNION " +
		"SELECT sum(nf_prod.valor_total) " +
		"FROM Produto_NotaFiscal nf_prod " +
		"JOIN Nota_Fiscal nf ON nf_prod.fk_Nota_Fiscal_id = nf.id " +
		"JOIN Destinatario d ON nf.fk_destinatario_id = d.id " +
		"JOIN UF ON d.uf_id = UF.id " +
		"WHERE UF.sigla = :sigla " +
		"GROUP BY UF.id";

		return jdbcTemplate.queryForStream(sql, Map.of("sigla", sigla), BeanPropertyRowMapper.newInstance(RetornoFloatDTO.class)).toList();

	}

	public List<NotaFiscalDTO> recuperarNotaFiscalCompleta(String chave) {
		String sql =
		"SELECT " +
		"nf.chave_de_Acesso AS chave, " +
		"nf.serie AS serie, " +
		"nf.numero AS numero, " +
		"nf.data_Emissao AS dataEmissao, " +
		"nf.destino_interno AS destinoInterno, " +
		"nf.valor AS valorTotal, " +
		"nf.consumidor_final AS consumidorFinal, " +
		"nop.descricao AS naturezaOperacao, " +
		"emit.CPF_CNPJ AS emitenteCPF_CNPJ, " +
		"emit.razao_Social AS emitenteRazaoSocial, " +
		"m.nome AS emitenteMunicipio, " +
		"UF.nome AS emitenteUF, " +
		"d.nome AS destinatarioNome, " +
		"d.cnpj AS destinatarioCNPJ, " +
		"pc.tipo_Presenca AS tipoPresenca " +
		"FROM Nota_Fiscal nf " +
		"JOIN Natureza_Operacao nop " +
		"ON nf.fk_natureza_operacao_id = nop.id " +
		"JOIN Emitente emit " +
		"ON nf.fk_emitente_id = emit.id " +
		"JOIN Municipio m " +
		"ON emit.municipio_id = m.id " +
		"JOIN UF " +
		"ON m.uf_id = UF.id " +
		"JOIN Destinatario d " +
		"ON nf.fk_destinatario_id = d.id " +
		"JOIN Presenca_Comprador pc " +
		"ON nf.fk_presenca_comprador_id = pc.id " +
		"WHERE nf.chave_de_Acesso = :chave " +
		"ORDER BY nf.chave_de_Acesso";

		List<NotaFiscalDTO> notasFiscaisDTO = jdbcTemplate.queryForStream(sql, Map.of("chave", chave), BeanPropertyRowMapper.newInstance(NotaFiscalDTO.class)).toList();

		String consultaUFDestinatario =
		"SELECT UF.nome AS destinatarioUF, nf.chave_de_Acesso AS chave FROM UF JOIN Destinatario d " +
		"ON d.uf_id = UF.id JOIN Nota_Fiscal nf " +
		"ON d.id = nf.fk_destinatario_id " +
		"WHERE nf.chave_de_Acesso = :chave " +
		"ORDER BY nf.chave_de_Acesso";

		List<NotaFiscalDTO> UFsDestinatarios = jdbcTemplate.queryForStream(consultaUFDestinatario, Map.of("chave", chave), BeanPropertyRowMapper.newInstance(NotaFiscalDTO.class)).toList();

		List<NotaFiscalDTO> notasFiscaisFinais = new ArrayList<>();

		for ( NotaFiscalDTO notaFiscal : notasFiscaisDTO) {
			Optional<NotaFiscalDTO> notaUF = UFsDestinatarios.stream()
					.filter(n -> n.getChave().equals(notaFiscal.getChave()))
					.findFirst();

			notaFiscal.setDestinatarioUF(notaUF.get().getDestinatarioUF());
			notasFiscaisFinais.add(notaFiscal);
		}

		return notasFiscaisFinais;
	}

	public List<ProdutoDTO> recuperarProdutosDeNotaFiscal(String chave) {
		String sql =
		"SELECT " +
		"p.descricao AS produto, " +
		"p.quantidade_unitaria AS quantidade, " +
		"p.unidade AS unidade, " +
		"p.valor_unitario AS valorUnitario, " +
		"prod_nf.valor_total AS valorTotal, " +
		"ncm.tipo_de_produto AS tipoProduto ," +
		"p.CFOP AS CFOP " +
		"FROM Produto p " +
		"JOIN NCM_SH_Tipo ncm " +
		"ON p.fk_codigo_NCM_SH = ncm.id " +
		"JOIN Produto_NotaFiscal prod_nf " +
		"ON p.id = prod_nf.fk_Produto_id " +
		"JOIN Nota_Fiscal nf " +
		"ON prod_nf.fk_Nota_Fiscal_id = nf.id " +
		"WHERE nf.chave_de_Acesso = :chave";

		return jdbcTemplate.queryForStream(sql, Map.of("chave", chave), BeanPropertyRowMapper.newInstance(ProdutoDTO.class)).toList();
	}
}
