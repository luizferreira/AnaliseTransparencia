package ufrj.br.analisetransparenciabackend.model.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotaFiscalDTO {
	
	private String chave;

	private String serie;

	private Integer numero;

	private Date dataEmissao;

	private boolean destinoInterno;

	private String valor;

	private boolean consumidorFinal;

	private String naturezaDaOperacao;

	private String evento;

	private String emitenteCPF_CNPJ;

	private String emitenteRazaoSocial;

	private String emitenteMunicipio;

	private String emitenteUF;

	private String destinatarioNome;

	private String destinatarioCNPJ;

	private String destinatarioUF;

	private String tipoPresenca;
	
	private Float valorTotal;

	private String Produto;
	

	

	


}
