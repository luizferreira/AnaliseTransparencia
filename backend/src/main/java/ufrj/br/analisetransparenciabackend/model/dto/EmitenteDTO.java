package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmitenteDTO {
	
	private String emitente;
	
	private String valorGasto;

}
