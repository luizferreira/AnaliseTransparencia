package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuantidadeNotasNaturezaoperacaoDTO {
	
	private String naturezaOperacao;
	
	private Integer quantidade;

}
