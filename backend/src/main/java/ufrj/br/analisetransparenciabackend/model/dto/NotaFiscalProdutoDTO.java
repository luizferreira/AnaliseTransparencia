package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NotaFiscalProdutoDTO {

    private NotaFiscalDTO notaFiscal;
    private List<ProdutoDTO> produtos;

}
