package ufrj.br.analisetransparenciabackend;

import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnaliseTransparenciaBackendApplication {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(AnaliseTransparenciaBackendApplication.class, args);
    }

}
