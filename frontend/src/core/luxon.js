import {DateTime} from "luxon";

export default {
    dataConverter(data){
        return DateTime.fromSQL(data).isValid === true ? DateTime.fromSQL(data).setLocale('br') : DateTime.fromJSDate(data).setLocale('br') ;
    },

    dataConverterFromIso(data){
        return DateTime.fromISO(data).setLocale('br');
    },

    local(){
        return DateTime.local().setLocale('br') ;
    },

    localDateToDateTime(data){
        return DateTime.fromFormat(data, "dd-LL-yyyy");
    }
}