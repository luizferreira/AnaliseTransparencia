import axios from "axios";

let url = 'http://localhost:8080/api/transparencia/';

export default {
    recuperarProdutosValorTotal(produto) {
        return axios.post(url + 'produtosComTotalGasto', {produto: produto});
    },

    recuperarDezEstadosQueMaisGastaram() {
        return axios.get(url + 'dezEstadosQueMaisGastaram');
    },

    recuperarNotasFiscaisPorProdutoDescricao(descricao) {
        return axios.get(url + 'notasFicais/produto/' + descricao);
    },

    recueprarQuantidadeNotasFiscaisEmitenteDestinatario(emitente, destinatario) {
        return axios.post(url + 'quantidadeNotasFiscaisEmitenteFezParaDestinatario', {
                emitente: emitente, destinatario: destinatario
            });
    },

    recuperarEmitentes(emitente) {
        return axios.post(url + 'emitente', {emitente: emitente});
    },

    recuperarDestinatarios(destinatario) {
        return axios.post(url + 'destinatario', {destinatario: destinatario});
    },

    recuperarValorGastoPorEmitente(emitente) {
        return axios.post(url + 'valorGastoPorEmitente', {emitente: emitente});
    },

    recuperarNomesNaturezaOperacaoAutocomplete(busca) {
        return axios.post(url + 'recuperarNomesNaturezaOperacaoAutocomplete', {busca: busca});   
    },

    recuperarQuantidadeNotasFiscaisPorNaturezaOperacao(naturezaOp) {
        return axios.post(url + 'quantidadeNotasFiscaisPorNaturezaOpercao', {naturezaOp: naturezaOp});
    },

    recuperarTotalPagoPorDestinatario(destinatario) {
        return axios.post(url + 'totalPagoPorDestinatario', {destinatario: destinatario});
    },
    recuperarMediaPorTipoProduto(tipoProduto) {
        return axios.post(url + 'mediaGastaPorTipoProduto', {tipoProduto: tipoProduto});
    },

    recuperarTiposProduto(tipoProduto) {
        return axios.post(url + 'recuperarTipoProduto', {tipoProduto: tipoProduto});
    },

    recuperarEstados() {
        return axios.get(url + 'recuperarEstados');
    },

    recuperarNotasPorPeriodo(dataInicio, dataFim) {
        return axios.post(url + 'notasPorPeriodo', {
            dataInicio: dataInicio, dataFim: dataFim
        });
    },

    recuperarValoresEmitidosERecebidosPorUF(sigla){
        return axios.post(url + 'valoresEmitidosERecebidos', {sigla: sigla})

    },

    recuperarNotaFiscalCompleta(chave){
        return axios.get(url + 'recuperarNotaFiscalCompleta/' + chave)
    }
}